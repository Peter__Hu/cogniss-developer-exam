const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputFile = "input2.json";
const outputFile = "output2.json";

// Read the file
var output = {};
jsonfile.readFile(inputFile, function (err, body) {
  if (err) {
    console.log(err);
    return;
  }
  console.log(body);
  output.document = body;
  // Loop through the array of names
  let names = output.document.names;
  names.forEach(
    (name, index) =>
      (names[index] = {
        name: names[index],
        emails: name
          .split("")
          .reverse()
          .join("")
          .concat(randomstring.generate(5) + "@gmail.com"),
      })
  );
  console.log(names);
  // Write the output into outout2.json
  jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
    if (err) throw err;
    console.log("All Done");
  });
});
